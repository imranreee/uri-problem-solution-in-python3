values1 = input("")
values2 = input("")

splitter1 = values1.split()
splitter2 = values2.split()

product1_code = int(splitter1[0])
product1_unit = int(splitter1[1])
product1_price = float(splitter1[2])

product2_code = int(splitter2[0])
product2_unit = int(splitter2[1])
product2_price = float(splitter2[2])

amount_to_be_paid = (product1_unit * product1_price) + (product2_unit * product2_price)
print("VALOR A PAGAR: R$ %.2f" % amount_to_be_paid)

