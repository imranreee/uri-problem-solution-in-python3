import math

radius = float(input())
volume = (4/3) * 3.14159 * (math.pow(radius, 3))

print("VOLUME = %0.3f" % volume)
