values = input("")

splitter = values.split()
value_of_a = float(splitter[0])
value_of_b = float(splitter[1])
value_of_c = float(splitter[2])
pi = 3.14159

area_of_the_rectangle_triangle = 0.5 * value_of_a * value_of_c
area_of_the_radius_circle = pi * value_of_c * value_of_c
area_of_the_trapezium = 0.5 * (value_of_a + value_of_b) * value_of_c
area_of_the_square = value_of_b * value_of_b
area_of_the_rectangle = value_of_a * value_of_b

print("TRIANGULO: %.3f" % area_of_the_rectangle_triangle)
print("CIRCULO: %.3f" % area_of_the_radius_circle)
print("TRAPEZIO: %.3f" % area_of_the_trapezium)
print("QUADRADO: %.3f" % area_of_the_square)
print("RETANGULO: %.3f" % area_of_the_rectangle)
