from math import sqrt

values1 = input("")
values2 = input("")

splitter1 = values1.split()
splitter2 = values2.split()

value_of_x1 = float(splitter1[0])
value_of_y1 = float(splitter1[1])

value_of_x2 = float(splitter2[0])
value_of_y2 = float(splitter2[1])

cal_x = pow((value_of_x2 - value_of_x1), 2)
cal_y = pow((value_of_y2 - value_of_y1), 2)

distance = sqrt(cal_x + cal_y)
print("%.4f" % distance)
