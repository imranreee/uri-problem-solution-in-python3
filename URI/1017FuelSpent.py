hours = int(input())
avg_speed = int(input())

total_km = hours * avg_speed
liters = float(total_km) / 12.00

print("%.3f" % liters)
