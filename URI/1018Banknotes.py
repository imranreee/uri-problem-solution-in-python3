amount = int(input())
amount_p = amount
how_many_hun = how_many_fif = how_many_twt = how_many_ten = how_many_five = how_many_two = how_many_one = 0

cal = int(amount / 100)
if cal >= 1:
    how_many_hun = cal
    amount = amount % 100

cal = int(amount / 50)
if cal >= 1:
    how_many_fif = cal
    amount = amount % 50

cal = int(amount / 20)
if cal >= 1:
    how_many_twt = cal
    amount = amount % 20

cal = int(amount / 10)
if cal >= 1:
    how_many_ten = cal
    amount = amount % 10

cal = int(amount / 5)
if cal >= 1:
    how_many_five = cal
    amount = amount % 5

cal = int(amount / 2)
if cal >= 1:
    how_many_two = cal
    amount = amount % 2

cal = int(amount / 1)
if cal >= 1:
    how_many_one = cal

print("%d" % amount_p)
print("%d nota(s) de R$ 100,00" % how_many_hun)
print("%d nota(s) de R$ 50,00" % how_many_fif)
print("%d nota(s) de R$ 20,00" % how_many_twt)
print("%d nota(s) de R$ 10,00" % how_many_ten)
print("%d nota(s) de R$ 5,00" % how_many_five)
print("%d nota(s) de R$ 2,00" % how_many_two)
print("%d nota(s) de R$ 1,00" % how_many_one)
