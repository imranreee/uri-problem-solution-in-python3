seconds = int(input())
hours = minutes = 0

cal = int(seconds / 3600)
if cal >= 1:
    hours = cal
    seconds = seconds % 3600

cal = int(seconds / 60)
if cal >= 1:
    minutes = cal
    seconds = seconds % 60

print(str(hours)+":"+str(minutes)+":"+str(seconds))
