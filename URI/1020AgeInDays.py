days = int(input())
years = months = 0

cal = int(days / 365)
if cal >= 1:
    years = cal
    days = days % 365

cal = int(days / 30)
if cal >= 1:
    months = cal
    days = days % 30

print("%d ano(s)" % years)
print("%d mes(es)" % months)
print("%d dia(s)" % days)
