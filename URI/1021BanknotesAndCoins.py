amount = float(input())
how_many_hun = how_many_fif = how_many_twt = how_many_ten = how_many_five = how_many_two = 0
how_many_one = fifty_coin = twenty_five_coin = ten_coin = five_coin = one_coin = 0

amount, amount_decimal = divmod(amount, 1)
amount_decimal = float("%.2f" % amount_decimal)

cal = int(amount / 100)
if cal >= 1:
    how_many_hun = cal
    amount = amount % 100

cal = int(amount / 50)
if cal >= 1:
    how_many_fif = cal
    amount = amount % 50

cal = int(amount / 20)
if cal >= 1:
    how_many_twt = cal
    amount = amount % 20

cal = int(amount / 10)
if cal >= 1:
    how_many_ten = cal
    amount = amount % 10

cal = int(amount / 5)
if cal >= 1:
    how_many_five = cal
    amount = amount % 5

cal = int(amount / 2)
if cal >= 1:
    how_many_two = cal
    amount = amount % 2

cal = int(amount / 1)
if cal >= 1:
    how_many_one = cal

amount_decimal = float("%.2f" % amount_decimal)
cal = int(amount_decimal / .50)
if cal >= 1:
    fifty_coin = cal
    amount_decimal = amount_decimal % .50

amount_decimal = float("%.2f" % amount_decimal)
cal = int(amount_decimal / .25)
if cal >= 1:
    twenty_five_coin = cal
    amount_decimal = amount_decimal % .25

amount_decimal = float("%.2f" % amount_decimal)
cal = int(amount_decimal / .10)
if cal >= 1:
    ten_coin = cal
    amount_decimal = amount_decimal % .10

amount_decimal = float("%.2f" % amount_decimal)
cal = int(amount_decimal / .05)
if cal >= 1:
    five_coin = cal
    amount_decimal = amount_decimal % .05

amount_decimal = float("%.2f" % amount_decimal)
cal = int(amount_decimal / .01)
if cal >= 1:
    one_coin = cal

print("NOTAS:")
print("%d nota(s) de R$ 100.00" % how_many_hun)
print("%d nota(s) de R$ 50.00" % how_many_fif)
print("%d nota(s) de R$ 20.00" % how_many_twt)
print("%d nota(s) de R$ 10.00" % how_many_ten)
print("%d nota(s) de R$ 5.00" % how_many_five)
print("%d nota(s) de R$ 2.00" % how_many_two)
print("MOEDAS:")
print("%d moeda(s) de R$ 1.00" % how_many_one)
print("%d moeda(s) de R$ 0.50" % fifty_coin)
print("%d moeda(s) de R$ 0.25" % twenty_five_coin)
print("%d moeda(s) de R$ 0.10" % ten_coin)
print("%d moeda(s) de R$ 0.05" % five_coin)
print("%d moeda(s) de R$ 0.01" % one_coin)
