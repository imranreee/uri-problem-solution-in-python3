from math import sqrt

values = input("")
splitter = values.split()

value_of_a = float(splitter[0])
value_of_b = float(splitter[1])
value_of_c = float(splitter[2])

cal = pow(value_of_b, 2) - (4 * value_of_a * value_of_c)

if (value_of_a == 0) or (value_of_b == 0) or (cal < 0):
    print("Impossivel calcular")
else:
    cal = sqrt(cal)
    cal1 = (- value_of_b + cal) / (2 * value_of_a)
    cal2 = (- value_of_b - cal) / (2 * value_of_a)
    print("R1 = %.5f" % cal1)
    print("R2 = %.5f" % cal2)
