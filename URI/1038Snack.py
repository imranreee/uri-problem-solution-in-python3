values = input("")
splitter = values.split()

value_of_x = int(splitter[0])
value_of_y = int(splitter[1])

if value_of_x == 1:
    amount_paid = float(value_of_y * 4.00)

elif value_of_x == 2:
    amount_paid = float(value_of_y * 4.50)

elif value_of_x == 3:
    amount_paid = float(value_of_y * 5.00)

elif value_of_x == 4:
    amount_paid = float(value_of_y * 2.00)

elif value_of_x == 5:
    amount_paid = float(value_of_y * 1.50)

print("Total: R$ %.2f" % amount_paid)
