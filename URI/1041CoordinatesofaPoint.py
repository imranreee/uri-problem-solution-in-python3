values = input("")
splitter = values.split()

value_of_x = float(splitter[0])
value_of_y = float(splitter[1])

if value_of_x == 0 and value_of_y == 0:
    print("Origem")
elif value_of_x == 0 and value_of_y != 0:
    print("Eixo Y")
elif value_of_y == 0 and value_of_x != 0:
    print("Eixo X")
elif value_of_x > 0 and value_of_y > 0:
    print("Q1")
elif (value_of_x < 0) and (value_of_y > 0):
    print("Q2")
elif value_of_x < 0 and value_of_y < 0:
    print("Q3")
elif (value_of_x > 0) and (value_of_y < 0):
    print("Q4")