values = input("")

splitter = values.split()
n1 = int(splitter[0])
n2 = int(splitter[1])
n3 = int(splitter[2])

if n1 > n2 and n1 > n3:
    maximum = n1
    if n2 > n3:
        mid = n2
        minimum = n3
    else:
        mid = n3
        minimum = n2
elif n2 > n1 and n2 > n3:
    maximum = n2
    if n3 > n1:
        mid = n3
        minimum = n1
    else:
        mid = n1
        minimum = n3
else:
    maximum = n3
    if n2 > n1:
        mid = n2
        minimum = n1
    else:
        mid = n1
        minimum = n2

print(minimum)
print(mid)
print(maximum)
print()
print(n1)
print(n2)
print(n3)
