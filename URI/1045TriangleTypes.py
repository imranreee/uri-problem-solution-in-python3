a, b, c = map(float, input().split())
num_list = [a, b, c]

num_list.sort(reverse=True)
a = num_list[0]
b = num_list[1]
c = num_list[2]

if a >= (b + c):
    print("NAO FORMA TRIANGULO")
else:
    if (a ** 2) == ((b ** 2) + (c ** 2)):
        print("TRIANGULO RETANGULO")
    if (a ** 2) > ((b ** 2) + (c ** 2)):
        print("TRIANGULO OBTUSANGULO")
    if (a ** 2) < ((b ** 2) + (c ** 2)):
        print("TRIANGULO ACUTANGULO")
    if a == b == c:
        print("TRIANGULO EQUILATERO")
    elif a == b:
        print("TRIANGULO ISOSCELES")
    elif a == c:
        print("TRIANGULO ISOSCELES")
    elif b == c:
        print("TRIANGULO ISOSCELES")
