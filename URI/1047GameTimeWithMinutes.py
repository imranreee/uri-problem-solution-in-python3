sh, sm, eh, em = map(int, input().split())

if sh == sm == eh == em:
    print("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)")

elif sh == eh and sm > em:
    duration_h = 23
    duration_m = int(60 - sm) + em
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh == eh and sm < em:
    duration_h = 0
    duration_m = em - sm
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh > eh and sm == em:
    duration_h = int(24 - sh) + eh
    duration_m = 0
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh < eh and sm == em:
    duration_h = eh - sh
    duration_m = 0
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh > eh and sm > em:
    duration_h = int((24 - sh) + eh) - 1
    duration_m = (60 - sm) + em
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh > eh and sm < em:
    duration_h = int(24 - sh) + eh
    duration_m = em - sm
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh < eh and sm < em:
    duration_h = eh - sh
    duration_m = em - sm
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))

elif sh < eh and sm > em:
    duration_h = (eh - sh) - 1
    duration_m = (60 - sm) + em
    print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duration_h, duration_m))
