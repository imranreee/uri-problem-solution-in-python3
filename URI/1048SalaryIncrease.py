salary = float(input())

if 0.00 <= salary <= 400.00:
    percent = 15
    earn = float(salary / 100.00) * float(percent)
    new_salary = salary + earn

elif 400.01 <= salary <= 800.00:
    percent = 12
    earn = float(salary / 100.00) * float(percent)
    new_salary = salary + earn

elif 800.01 <= salary <= 1200.00:
    percent = 10
    earn = float(salary / 100.00) * float(percent)
    new_salary = salary + earn

elif 1200.01 <= salary <= 2000.00:
    percent = 7
    earn = float(salary / 100.00) * float(percent)
    new_salary = salary + earn

elif salary > 2000.00:
    percent = 4
    earn = float(salary / 100.00) * float(percent)
    new_salary = salary + earn

print("Novo salario: %.2f" % new_salary)
print("Reajuste ganho: %.2f" % earn)
print("Em percentual: " + str(percent) + " %")
