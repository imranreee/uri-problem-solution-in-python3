salary = float(input())

if 0.00 <= salary <= 2000.00:
    print("Isento")

else:
    if salary >= 4500.01:
        tax = float(salary - 4500.00)
        tax = float((tax / 100.00) * 28.00)
        tax = float(350.00 + tax)

    elif 3000.01 <= salary <= 4500.00:
        tax = float(salary - 3000.00)
        tax = float((tax / 100.00) * 18.00)
        tax = float(80.00 + tax)

    elif 2000.01 <= salary <= 3000.00:
        tax = float(salary - 2000.00)
        tax = float((tax / 100.00) * 8.00)

    print("R$ %.2f" % tax)
