val1 = input().split()
text1 = str(val1[0])
val1 = int(val1[1])
values1 = input().split(":")
sh = int(values1[0])
sm = int(values1[1])
ss = int(values1[2])

val2 = input().split()
text2 = str(val2[0])
val2 = int(val2[1])
values2 = input().split(":")
eh = int(values2[0])
em = int(values2[1])
es = int(values2[2])

days = int(val2 - val1)

hours = eh - sh
if hours < 0:
    hours = int(24 + hours)
    days = int(days - 1)

minutes = em - sm
if minutes < 0:
    minutes = int(60 + minutes)
    hours = int(hours - 1)

seconds = es - ss
if seconds < 0:
    seconds = int(60 + seconds)
    minutes = int(minutes - 1)

if days <= 0:
    days = 0

print("%d dia(s)" % days)
print("%d hora(s)" % hours)
print("%d minuto(s)" % minutes)
print("%d segundo(s)" % seconds)
