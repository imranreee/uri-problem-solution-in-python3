numbers = []
counter_even = 0
counter_odd = 0
counter_positive = 0
counter_negative = 0

numbers.append(int(input()))
numbers.append(int(input()))
numbers.append(int(input()))
numbers.append(int(input()))
numbers.append(int(input()))

for i in numbers:
    if i % 2 == 0 or i % 2 == -0:
        counter_even += 1
    if i % 2 != 0 or i % 2 != -0:
        counter_odd += 1
    if i > 0:
        counter_positive += 1
    if i < 0:
        counter_negative += 1

print("%d valor(es) par(es)" % counter_even)
print("%d valor(es) impar(es)" % counter_odd)
print("%d valor(es) positivo(s)" % counter_positive)
print("%d valor(es) negativo(s)" % counter_negative)
