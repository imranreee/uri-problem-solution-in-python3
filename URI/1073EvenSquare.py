value = int(input())

for i in range(1, value+1):
    if i % 2 == 0:
        cal = pow(i, 2)
        print(str(i) + "^2 = " + str(cal))
