how_many = int(input())
values = []

for i in range(how_many):
    values.append(int(input()))

for i in values:
    if i == 0:
        print("NULL")

    elif i > 0:
        if i % 2 == 0:
            print("EVEN POSITIVE")
        else:
            print("ODD POSITIVE")

    else:
        if i % 2 == 0:
            print("EVEN NEGATIVE")
        else:
            print("ODD NEGATIVE")

