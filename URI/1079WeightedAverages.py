how_many = int(input())

for i in range(how_many):
    n1, n2, n3 = map(float, input().split())
    avg = float((2 * n1) + (3 * n2) + (5 * n3)) / float(2 + 3 + 5)
    print("%.1f" % avg)
