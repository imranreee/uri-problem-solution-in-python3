how_many = int(input())
coelho  = 0
rato = 0
sapo = 0
total = 0

for i in range(how_many):
    values = input("")
    splitter1 = values.split()

    number = int(splitter1[0])
    character = str(splitter1[1])
    total += number

    if character == 'C':
        coelho += number
    elif character == 'R':
        rato += number
    elif character == 'S':
        sapo += number

coelho_per = float((100 / total) * coelho)
rato_per = float((100 / total) * rato)
sapo_per = float((100 / total) * sapo)

print("Total: %d cobaias" % total)
print("Total de coelhos: %d" % coelho)
print("Total de ratos: %d" % rato)
print("Total de sapos: %d" % sapo)
print("Percentual de coelhos: %.2f %%" % coelho_per)
print("Percentual de ratos: %.2f %%" % rato_per)
print("Percentual de sapos: %.2f %%" % sapo_per)
