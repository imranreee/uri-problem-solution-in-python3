while True:
    sum = 0
    m, n = map(int, input().split())

    if m <= 0 or n <= 0:
        break

    elif m > n:
        for i in range(n, m+1):
            print(i, end=' ')
            sum += i
        print("Sum=" + str(sum))

    else:
        for i in range(m, n+1):
            print(i, end=' ')
            sum += i
        print("Sum=" + str(sum))
