how_many = int(input())
count = 0

while count < how_many:
    count += 1

    value_of_x, value_of_y = map(int, input().split())

    if value_of_y == 0:
        print("divisao impossivel")
    else:
        print("%.1f" % (value_of_x/value_of_y))


