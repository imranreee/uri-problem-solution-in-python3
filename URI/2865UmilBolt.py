how_many = int(input())
minimum = 100.00

for i in range(how_many):
    seconds = float(input())

    if seconds < minimum:
        minimum = seconds

print("%.2f" % minimum)
